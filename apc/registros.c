
#include <stdio.h>
#include <string.h>

typedef struct{
    char rua[20];
    int num;
} TipoEndereco;

typedef struct {
    int matricula;
    char nome[20];
    TipoEndereco endereco;
} Aluno;


void read(Aluno *ptr, int tamanho){

    for(int i = 0; i < tamanho; i++){
        scanf("%d", &ptr[i].matricula);
        scanf(" %[^\n]s", ptr[i].nome);
        scanf(" %[^\n]s", ptr[i].endereco.rua );
        scanf("%d", &ptr[i].endereco.num );
    }
}


int main(){
    Aluno alunos[100];
    int n;

    scanf("%d", &n);

    read(alunos, n);

    printf("\n\n Valores lidos!\n");
    for(int i =0; i < n; i++){
        printf("%d\n", alunos[i].matricula);
        printf("%s\n", alunos[i].nome);
        printf("%s\n", alunos[i].endereco.rua);
        printf("%d\n", alunos[i].endereco.num);
    }


    return 0;

}




